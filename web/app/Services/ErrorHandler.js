class ErrorHandler extends Error{
  constructor(error, response) {
    super();
    this.error = error;
    this.res = response;
  }

  execute() {
    if (this.error.isJoi) {
      this.res.status(400).send({ success: false, message: this.error.message })
    } else if(this.error.name.indexOf('Sequelize') !== -1) {
      if(this.error.parent && this.error.parent.errno === 1062) {
        this.res.status(500).send({
          success: false,
          message: 'This instance already existed',
          type: this.error.errors[0].type
        })
      } else {
        this.res.status(500).send({
          success: false,
          error: this.error.message
        })
      }
    } else {
      this.res.status(500).send({
        success: false,
        message: 'Server error',
        full: {
          name: this.error.name,
          message: this.error.message,
          stack: this.error.stack
        },
        error: this.error
      })
    }
  }
}

module.exports = ErrorHandler;
