module.exports = function (sequelize, Sequelize) {

    let TransactionsSchema = {
        inv_id: {
            type: Sequelize.BIGINT,
            references: {
                model: 'inventory',
                key: 'id'
            },
            primaryKey: true
        },
        type_id: {
            type: Sequelize.INTEGER,
            references: {
                model: 'statuses',
                key: 'id'
            },
            primaryKey: true
        },
        name:{
          type: Sequelize.STRING
        },
        created_at: {
          type: Sequelize.DATE,
          primaryKey: true
        },
        deleted_at: Sequelize.DATE
    };

    let ModelOptions = {
      paranoid: true,
      deletedAt: 'deleted_at',
      createdAt: 'created_at',
      freezeTableName: true,
      updatedAt: false
    };

    let Transactions = sequelize.define('transactions', TransactionsSchema, ModelOptions);

    Transactions.prototype.prettify = function () {
        return {
            ...this.dataValues,
            inv_id: `G${this.inv_id}`
        }
    };

    return Transactions;
};

