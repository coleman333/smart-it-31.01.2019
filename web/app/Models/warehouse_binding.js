module.exports = (sequelize, Sequelize)=>{

    const WarehousesTransActionsSchema = {
        id: {
            autoIncrement: true,
            unique: true,
            allowNull: false,
            primaryKey: true,
            type: Sequelize.BIGINT
        },
        warehouses_id: {
            type: Sequelize.INTEGER,
            reference: {
                model: 'warehouse',
                key: 'id'
            },
            primaryKey:true,
            defaultValue: 1
        },
        inventory_id:{
            type: Sequelize.INTEGER,
            reference: {
                model: 'inventory',
                key: 'id',
                unique: true,
            }
        },
        date:{
            type: Sequelize.DATE,
            defaultValue: Date.now()
        }

    };

    const ModelOptions = {
        timestamps: false,
        paranoid: true,
        // deletedAt: 'deleted_at',
        // createdAt: 'created_at',
        freezeTableName: true,
        // updatedAt: false
    };

    let WarehouseBinding = sequelize.define('warehouse_binding',WarehousesTransActionsSchema,ModelOptions);

    return WarehouseBinding;
};
