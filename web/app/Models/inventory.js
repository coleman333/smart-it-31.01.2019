module.exports = function (sequelize, Sequelize) {

    let InventorySchema = {
        id: {
            autoIncrement: true,
            unique: true,
            allowNull: false,
            primaryKey: true,
            type: Sequelize.BIGINT
        },
        metadata_id: {
            type: Sequelize.BIGINT,
            references: {
                model: 'inventory_metadata',
                key: 'id'
            }
        },
        // warehouse_id: {
        //     type: Sequelize.INTEGER,
        //     references: {
        //         model: 'warehouse_binding',
        //         key: 'id'
        //     }
        // },
        is_marked: {
            type: Sequelize.BOOLEAN,
            default: false
        },
        status_id: {
            type: Sequelize.INTEGER,
            references: {
                model: 'statuses',
                key: 'id'
            }
        },
        equipment_id: {
            type: Sequelize.BIGINT,
            references: {
                model: 'equipment',
                key: 'id'
            },
            default: null
        },
        created_at: {
            type: Sequelize.DATE
        },
        deleted_at: {
            type: Sequelize.DATE
        },
    };

    let ModelOptions = {
        paranoid: true,
        deletedAt: 'deleted_at',
        createdAt: 'created_at',
        freezeTableName: true,
        updatedAt: false
    };

    const Inventory = sequelize.define('inventory', InventorySchema, ModelOptions);


    Inventory.getQuantity = function (consignment) {
        return sequelize.query(`
        SELECT
        *
        FROM (
          SELECT
            i.metadata_id,
            COUNT(i.id) AS amount,
            GROUP_CONCAT(i.id) AS ids
          FROM inventory AS i
          LEFT JOIN inventory_metadata AS im ON im.id = i.metadata_id
          WHERE im.consignment = consignment AND i.is_marked = FALSE 
          GROUP BY i.metadata_id
        ) AS I
        LEFT JOIN inventory_metadata AS IM ON IM.id = I.metadata_id;
			`, {
            replacements: {
                consignment: consignment+'%'
            },
            type: sequelize.QueryTypes.SELECT
        })
    };

    Inventory.getInventoryQuantity = function () {
        return sequelize.query(`
        SELECT 
        SUM(amount) as totalInventory
        from(
          SELECT
            COUNT(i.id) AS amount,
            GROUP_CONCAT(i.id) AS ids
          FROM inventory AS i
          LEFT JOIN inventory_metadata AS im ON im.id = i.metadata_id
          GROUP BY i.metadata_id
          ) AS I
			`, {
            type: sequelize.QueryTypes.SELECT
        })
    };

    Inventory.getInventoryQuantityMounted = function () {
        return sequelize.query(`
       SELECT 
        SUM(amount) as mountedInventory
        from(
          SELECT
            COUNT(i.id) AS amount,
            GROUP_CONCAT(i.id) AS ids
          FROM statuses, inventory AS i
          LEFT JOIN inventory_metadata AS im ON im.id = i.metadata_id
          WHERE i.status_id = 1 AND i.status_id = statuses.id 
          GROUP BY i.metadata_id
          ) AS I
			`, {
            type: sequelize.QueryTypes.SELECT
        })
    };

    Inventory.getInventoryQuantityUnMounted = function () {
        return sequelize.query(`
       SELECT 
        SUM(amount) as unMountedInventory
        from(
          SELECT
            COUNT(i.id) AS amount,
            GROUP_CONCAT(i.id) AS ids
          FROM statuses, inventory AS i
          LEFT JOIN inventory_metadata AS im ON im.id = i.metadata_id
          WHERE i.status_id = 2 AND i.status_id = statuses.id 
          GROUP BY i.metadata_id
          ) AS I
			`, {
            type: sequelize.QueryTypes.SELECT
        })
    };

    Inventory.prototype.prettify = function () {
        if (this.metadata) {
            const keysMetadata = [
                'Документ матеріалу',
                 'Документ закупівлі',
                 'Дата проводки',
                 'Наименование поставщика',
                 'Приходный ордер',
                 'Завод',
                 'Місце збереження',
                 'Партія',
                 'Матеріал',
                 'Опис матеріалу',
                 'Базис.один.виміру',
                 'Кількість',
                 'Елемент ІСР',
                 'Опис'
            ];
               const valuesMetadata = [
                    this.metadata.doc_material,
                    this.metadata.doc_purchasing,
                    this.metadata.check_date,
                    this.metadata.provider_name,
                    this.metadata.income_order,
                    this.metadata.factory,
                    this.metadata.store_place,
                    this.metadata.consignment,
                    this.metadata.material,
                    this.metadata.title,
                    this.metadata.evaluation_unit,
                    this.metadata.quantity,
                    this.metadata.ICP,
                    this.metadata.inventorization,

                ];
                // meta = JSON.parse(this.metadata.metadata);
            // console.log('+++++++++',this.metadata,'=========')
            // console.log('+++++++++',meta,'=========');
            // console.log('+++++++++',this.metadata,'=========')
            // Object.keys(this.metadata).forEach((item) => {
            //     keysMetadata.push(item);
            //     valuesMetadata.push(this.metadata[item]);
            // });
        //     console.log('+++++++++',keysMetadata,'=========')
            return {
                id: `G${this.id}`,
                title: this.metadata.title,
                consignment: this.metadata.consignment,
                material: this.metadata.material,
                is_marked: this.is_marked,
                metadata: {keysMetadata, valuesMetadata},
                status: this.status,
                equipment: this.equipment ? this.equipment.prettify() : undefined,
                transactions: this.transactions
            };
        } else {
            // TODO: add more fields
            return {
                id: `G${this.id}`,
                is_marked: this.is_marked,
            };
        }
    };

    return Inventory;
};

