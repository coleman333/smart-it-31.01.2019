
module.exports = function (sequelize,Sequelize) {

    let EquipmentSchema = {
        id: {
            autoIncrement: true,
            unique: true,
            allowNull: false,
            primaryKey: true,
            type: Sequelize.BIGINT
        },
        name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        equipment_id: {
            type: Sequelize.STRING,
            allowNull: false
        },
        is_marked: {
            type: Sequelize.BOOLEAN,
            default: false
        },
        inventory_id: {
            type: Sequelize.BIGINT,             //changed string on bigInt
            allowNull: false
        },
        created_at: {
            type: Sequelize.DATE
        },
        deleted_at: {
            type: Sequelize.DATE
        }
    };

    let ModelOptions = {
        paranoid: true,
        deletedAt: 'deleted_at',
        createdAt: 'created_at',
        freezeTableName: true,
        updatedAt: false
    };

    const Equipment = sequelize.define('equipment', EquipmentSchema, ModelOptions);

    // Equipment.getEquipmentQuantity = function () {
    //     return sequelize.query(`
    //     SELECT
    //     SUM(amount) as totalEquipment
    //     from(
    //       SELECT
    //         COUNT(i.id) AS amount,
    //         GROUP_CONCAT(i.id) AS ids
    //
    //       FROM inventory AS i
    //       LEFT JOIN inventory_metadata AS im ON im.id = i.metadata_id
    //       GROUP BY i.metadata_id
    //       ) AS I
	// 		`, {
    //         type: sequelize.QueryTypes.SELECT
    //     })
    // };

    Equipment.prototype.prettify = function() {
        return {
            id: `E${this.id}`,
            name: this.name,
            is_marked: this.is_marked,
            unit_equipment: this.equipment_id,
            inventory_number: this.inventory_id,
            inventory: this.inventory ? this.inventory.map(inv => inv.prettify()) : undefined
        };
    };



    return Equipment;
};

