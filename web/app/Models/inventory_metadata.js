module.exports = function (sequelize,Sequelize) {

    let InventoryMetadataSchema = {
        id: {
            autoIncrement: true,
            unique: true,
            allowNull: false,
            primaryKey: true,
            type: Sequelize.BIGINT
        },
        doc_material:{
            type: Sequelize.STRING,
        },
        doc_purchasing:{
            type: Sequelize.STRING,
        },
        check_date:{
            type: Sequelize.DATE,
        },
        provider_name:{
            type: Sequelize.STRING,
        },
        income_order:{
            type: Sequelize.STRING,
        },
        factory:{
            type: Sequelize.STRING,
        },
        store_place:{
            type: Sequelize.STRING,
        },
        consignment:{
            type: Sequelize.STRING,
        },
        material:{
            type: Sequelize.STRING,
        },
        title:{
            type: Sequelize.STRING,
        },
        evaluation_unit:{
            type: Sequelize.STRING,
        },
        quantity:{
            type: Sequelize.STRING,
        },
        ICP:{
            type: Sequelize.STRING,
        },
        inventorization:{
            type: Sequelize.STRING,
        },

        // title: {
        //     type: Sequelize.STRING,
        //     allowNull: false
        // },
        // consignment: {
        //     type: Sequelize.STRING,
        //     allowNull: false
        // },
        // material: {
        //     type: Sequelize.INTEGER,
        //     allowNull: false
        // },
        // metadata:{
        //     type: Sequelize.JSON,
        //     default: null
        // },
        created_at: {
            type: Sequelize.DATE
        }
    };

    let ModelOptions = {
        paranoid: true,
        deletedAt: 'deleted_at',
        createdAt: 'created_at',
        freezeTableName: true,
        updatedAt: false
    };


    return sequelize.define('inventory_metadata', InventoryMetadataSchema, ModelOptions);
};

