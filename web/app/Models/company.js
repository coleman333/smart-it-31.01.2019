module.exports= (sequelize,Sequelize)=>{

    const companySchema = {
        id:{
            autoIncrement: true,
            unique: true,
            allowNull: false,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        company_name:{
            type: Sequelize.STRING,
            defaultValue: 'default company'
        }
    };

    const ModelOptions = {
        timestamps: false,
        paranoid: true,
        // deletedAt: 'deleted_at',
        // createdAt: 'created_at',
        freezeTableName: true,
        // updatedAt: false
    };

    let company = sequelize.define('company',companySchema, ModelOptions);
    return company;
}
