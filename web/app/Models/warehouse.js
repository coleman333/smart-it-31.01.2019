module.exports = (sequelize,Sequelize)=>{

    const WarehouseSchema = {
        id: {
            autoIncrement: true,
            unique: true,
            allowNull: false,
            primaryKey: true,
            type: Sequelize.BIGINT
        },
        warehouse_name:{
            type: Sequelize.STRING
        },
        location:{
            type: Sequelize.STRING,
            defaultValue: 'default location'
        },
        company_id:{
          type: Sequelize.INTEGER,
          reference:{
              model: 'company',
              key: 'id'
          }
        },
        storekeeper_name:{
            type: Sequelize.STRING,
            defaultValue: 'default name'
        },
        storekeeper_second_name:{
            type: Sequelize.STRING,
            defaultValue: 'default second name'
        },
        macAddress:{
            type: Sequelize.STRING
        }
    };

    let ModelOptions = {
        timestamps: false,
        paranoid: true,
        // deletedAt: 'deleted_at',
        // createdAt: 'created_at',
        freezeTableName: true,
        // updatedAt: false
    };

    let Warehouse = sequelize.define('warehouse', WarehouseSchema, ModelOptions);

    return Warehouse;
};
