
module.exports = function (sequelize,Sequelize) {

    let StatusesSchema = {
        name: Sequelize.STRING
    };

    let ModelOptions = {
        timestamps: false
    };

    return sequelize.define('statuses', StatusesSchema, ModelOptions);
};

