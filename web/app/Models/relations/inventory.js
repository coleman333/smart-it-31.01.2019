module.exports = function(db) {
  db.inventory.belongsTo(db.equipment, { foreignKey: 'equipment_id', as: 'equipment' });
  db.inventory.belongsTo(db.statuses, { foreignKey: 'status_id', as: 'status' });
  db.inventory.belongsTo(db.inventory_metadata, { foreignKey: 'metadata_id', as: 'metadata' });
  db.inventory.hasMany(db.transactions, { foreignKey: 'inv_id', as: 'transactions' });
  db.equipment.hasMany(db.inventory, { foreignKey: 'equipment_id', as: 'inventory' });

  db.inventory.belongsTo(db.warehouse_binding,{ foreignKey: 'id', as: 'warehouse_binding'});
  db.warehouse_binding.hasMany(db.warehouse,{ foreignKey: 'warehouses_id', as: 'warehouse'});
  db.warehouse.belongsTo(db.company,{foreignKey:'company_id',as:'company'})
};



