module.exports = function(db) {
  db.transactions.belongsTo(db.inventory, { foreignKey: 'inv_id', as: 'inventory' });
  db.transactions.belongsTo(db.statuses, { foreignKey: 'type_id', as: 'status' });
};
