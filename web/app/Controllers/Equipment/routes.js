const express = require('express');
const Router = express.Router();
const EquipClass = require('./Equipment');
const Equipment = new EquipClass();

Router.get('/marked-unmarked/:is_marked',Equipment.getEquipmentByMark.bind(Equipment));
Router.put('/mark/:id', Equipment.markEquipment.bind(Equipment));
Router.get('/count-all',Equipment.countAll.bind(Equipment));
Router.get('/by-unit-number/:unit_equipment',Equipment.equipmentByUnitNumber.bind(Equipment));


Router.get('/', Equipment.getAll.bind(Equipment));
Router.get('/:id', Equipment.get.bind(Equipment));
Router.delete('/:id', Equipment.delete.bind(Equipment));

Router.post('/', Equipment.create.bind(Equipment));
Router.post('/:id/inventory', Equipment.assignInventory.bind(Equipment));
Router.put('/:id', Equipment.update.bind(Equipment));


module.exports = Router;

