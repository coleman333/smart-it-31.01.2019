const Controller = require('../Controller');
const GlobalModel = require('@model/index');
const Equipment = GlobalModel.equipment;
const Inventory = GlobalModel.inventory;
const Status = GlobalModel.statuses;

const Schema = require('@schema/Equipment');

class EquipmentController extends Controller {
  create(Request, Response, next) {
    this.Joi.validate(Request.body, Schema.create).then((data) => {
      Equipment.create(data).then((equipment) => {
        Response.send(equipment);
      }).catch((error) => {
        next(error);
      })
    }).catch((error) => {
      next(error);
    })
  }

  countAll(Request, Response, next){
    Equipment.count({
      }).then((equipmentCount)=>{
         Response.json({equipmentCount})
      })
  }

  async getEquipmentByMark(Request, Response, next){
    // const is_marked  = Request.params.is_marked;
    console.log("+++++++++++++++++++");
    // let equipment = await Equipment.findAll({where:{is_marked:Request.params.is_marked}});
    let equipment = await Equipment.findAll({where:{ is_marked: Request.params.is_marked.toLowerCase() !== 'false' }});

    console.log("+++++++++++++++++++",equipment);
    // Response.send(equipment.prettify())
    Response.json(equipment);

  }

  async equipmentByUnitNumber(Request, Response, next){
    let equipment = await Equipment.find({where:{equipment_id:Request.params.unit_equipment}});
    Response.send(equipment.prettify())
  }

  get(Request, Response, next) {
    Equipment.findById(Request.params.id, {
      include: [
        {
          model: Inventory,
          as: 'inventory',
          include: [
            {
              model: Status,
              as: 'status'
            }
          ]
        }
      ]
    }).then((equipments) => {
      Response.send(equipments);
    }).catch((error) => {
      next(error);
    })
  }

  async markEquipment(Request, Response, next){
    const id = Request.params.id.replace(/^\D/, '');
    const equipment = await Equipment.findByPk(id);
    if (!equipment) {
      Response.status(404).send({success: false, message: 'Equipment does not found'});
      return next();
    }
    if (equipment.is_marked) {
      Response.status(406).send({success: false, message: 'Equipment already marked'});
      return next();
    }
    equipment.is_marked = true;
    await equipment.save();
    Response.send(equipment.prettify());

    return next();
  }

  getAll(Request, Response, next) {
    Equipment.findAll({
      include: [
        {
          model: Inventory,
          as: 'inventory'
        }
      ]
    }).then((equipments) => {
      Response.send(equipments);
    }).catch((error) => {
      next(error);
    })
  }

  assignInventory(Request, Response, next) {
    if(!Request.body.inv_id) {
      Response.status(400).send({ success: false, message: '`inv_id` is required parameter' });
      return;
    }
    const inventory_id = Request.body.inv_id.slice(1);
    // Inventory.findById(Request.body.inv_id)
    Inventory.findById(inventory_id)
      .then(async (inventory) => {
        if(!inventory) {
          Response.status(404).send({ success: false, message: '`inventory not found' });
          return;
        }
        if(Request.params.id === 'quipment0'){
          console.log('+++++++++++++++++++++++++++',Request.params.id);
          inventory.update({
            equipment_id: '100000',
            // equipment_id: Request.params.id,
          });
          Response.send(inventory);
        }
        else{
          const equipment_id = Request.params.id.slice(1);
          // if(equipment_id === 'quipment0'){
          //   console.log('+++++++++++++++++++++++++++',equipment_id)
          //   inventory.update({
          //     equipment_id: '100000',
          //     // equipment_id: Request.params.id,
          //   });
          // }
          // else {
          inventory.update({
            equipment_id: equipment_id,
            // equipment_id: Request.params.id,
          });
          // }
          Response.send(inventory);
        }

      }).catch((error) => {
        next(error);
      })
  }

  update(Request, Response, next) {
    this.Joi.validate(Request.body, Schema.update).then((data) => {
      Equipment.findById(Request.params.id).then((equipment) => {
        if (!equipment) {
          Response.status(404).send({ success: false, message: 'Equipment does not found' })
          return;
        }
        equipment.update(data);
        Response.send(equipment);
      })
    }).catch((error) => {
      next(error);
    })
  }

  delete(Request, Response, next) {
    Equipment.findById(Request.params.id).then((equipment) => {
      if (!equipment) {
        Response.status(404).send({ success: false, message: 'Equipment does not found' });
        return;
      }
      equipment.destroy();
      Response.send(equipment);
    }).catch((error) => {
      next(error);
    })
  }
}
module.exports = EquipmentController;
