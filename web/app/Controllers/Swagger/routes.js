const express = require('express');
const Router = express.Router();
const swagger = require('swagger-ui-express');
const YAML = require('yamljs');
const equipmentFile = YAML.load(__dirname + '/../Equipment/routes.yaml');
const deviceFile = YAML.load(__dirname + '/../Device/routes.yaml');
const inventoryFile = YAML.load(__dirname + '/../Inventory/routes.yaml');

Router.use('/device', swagger.serve, (req, res, next) => swagger.setup(deviceFile)(req, res, next));
Router.use('/equipment', swagger.serve, (req, res, next) => swagger.setup(equipmentFile)(req, res, next));
Router.use('/inventory', swagger.serve, (req, res, next) => swagger.setup(inventoryFile)(req, res, next));

module.exports = Router;

