const express = require('express');
const Router = express.Router();
const Device = require('@controller/Device/device');
const DeviceController = new Device();

Router.post('/', DeviceController.createConnection.bind(DeviceController));

module.exports = Router;
