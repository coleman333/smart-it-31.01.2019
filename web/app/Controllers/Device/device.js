const Controller = require('@controller/Controller');

const net = require('net');

class DeviceController extends Controller{

    createConnection(req, res, next){
        let client = new net.Socket();
        const { port, host, message } = req.body;

        client.connect(port, host, () => {
            client.write( message, 'utf8');
        });

        client.on('data', (data) => {
            res.send({ message: data.toString() });
            client.destroy(); // kill client after server's response
        });

        client.on('error', (error) => {
            res.status(500).send({ success: false, message: error.message })
        });
    }
}

module.exports = DeviceController;
