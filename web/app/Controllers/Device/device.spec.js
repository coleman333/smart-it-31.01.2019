const expect = require('chai').expect;
const EventEmitter = require('events').EventEmitter;

const mock = require('mock-require');

describe('Server', function () {

    after(() => {
        mock.stop('net')
    });

    it('Should smthg', function (done) {
        const response = 'some message';
        const req = {
            body: {
                host: 1,
                port: 2,
                message: 'hello'
            }
        };
        const res = {
            send(data) {
                expect(data).to.be.an('object').that.has.property('message', response)
            },
            status() {
                return this
            }
        };

        class Socket extends EventEmitter {
            connect(port, host, cb) {
                expect(host).to.be.equal(req.body.host);
                expect(port).to.be.equal(req.body.port);
                cb();
            }

            write(message, encoding) {
                expect(encoding).to.be.equal('utf8');
                expect(message).to.be.equal(req.body.message);
                setTimeout(() => this.emit('data', response));
            }

            destroy() {
                done();
            }
        }

        const net = {
            Socket: Socket
        };

        mock('net', net);
        const DeviceController = require('./device');
        const middleware = new DeviceController().createConnection;

        middleware(req, res);
    });
});