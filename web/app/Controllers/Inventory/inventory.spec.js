const assert = require('chai').assert;
const expect = require('chai').expect;
const before = require('mocha').before;
const after = require('mocha').after;

const database = require('../../Models/connection');
const migrations = require('./../../../database');
const xlxsParserForTests = require('../../../tests/xlxsParserForTests');

const Controller = require('@controller/Controller');
const GlobalModel = require('@model/index');
const Inventory = GlobalModel.inventory;
const Equipment = GlobalModel.equipment;
const Statuses = GlobalModel.statuses;
const Transaction = GlobalModel.transactions;
const InventoryController = require('../../Controllers/Inventory/Inventory');

// const Inventory = sequelize.define('inventory', InventorySchema, ModelOptions);

// expect(InventoryController.getQuantity).to.have.property(consignment).with.lengthOf(6);

class ResponseMock {
    constructor(cb) {
        this.responseStatus = 200;
        this.responseObject = null;
        this.cb = cb;
    }

    status(status) {
        this.responseStatus = status;
        return this;
    }

    send(obj) {
        this.responseObject = JSON.parse(JSON.stringify(obj));
        if (typeof this.cb === 'function') {
            this.cb(this);
        }
        return this;
    }
}


describe("inventory tests", function () {
    let s;
    before(async () => {
        console.log("Начало тестов");
        s = database.sequelize;

        await s.query('DROP TABLE IF EXISTS transactions, inventory, equipment, inventory_metadata, statuses');
        const keyList = Object.keys(migrations);
        for (let key of keyList) {
            await s.query(migrations[key].up);
        }
    });

    after(async function () {
        // await s.query('DROP TABLE transactions, inventory, equipment, inventory_metadata, statuses');
        console.log("Конец тестов");
    });



    it("add resource to db",async function () {
        let xlxsParser = new xlxsParserForTests();
        await xlxsParser.uploadFile();
        const id = 100001;

        let obj;
        await new Promise(resolve => {
            const resp = new ResponseMock((_obj) => {
                obj = _obj;
                resolve();
            });

            const InvController = new InventoryController();
            InvController.get({params: {id}}, resp);
        });

        expect(obj.responseStatus).to.be.eql(200);
        expect(obj.responseObject).to.have.property('id');
        expect(obj.responseObject.id).to.be.eql(id);
        console.log(obj.responseObject);
    });




    it("возводит в n-ю степень", function (done) {
        expect(2).to.be.eql(2);
        done();
        // expect(InventoryController.getQuantity).to.be.a(undefined);
    });

});