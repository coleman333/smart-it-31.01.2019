const express = require('express');
const Router = express.Router();
const InvClass = require('@controller/Inventory/Inventory');
const Inventory = new InvClass();
const TransactionClass = require('@controller/Inventory/Transactions');
const Transactions = new TransactionClass();


Router.post('/inventory',Inventory.getAllWithFilterWithPagination.bind(Inventory));   //route filter with pagination
Router.get('/:consignment/unmarked',Inventory.getQuantity.bind(Inventory));
Router.get('/count-all',Inventory.countAll.bind(Inventory));
Router.get('/count-all-objects',Inventory.countAllObjects.bind(Inventory));
Router.get('/count-all-mounted',Inventory.countAllMounted.bind(Inventory));
Router.get('/count-all-unmounted',Inventory.countAllUnMounted.bind(Inventory));
Router.get('/count-all-marked',Inventory.countAllMarked.bind(Inventory));
Router.get('/count-all-unmarked',Inventory.countAllUnMarked.bind(Inventory));
Router.put('/inventory-bind-warehouse',Inventory.bindInventoryToWareHouse.bind(Inventory));


Router.get('/:id', Inventory.get.bind(Inventory));
Router.get('/:id/servedate', Inventory.getServeDate.bind(Inventory));

Router.put('/:id/mark',Inventory.markInventory.bind(Inventory));
Router.get('/', Inventory.getAll.bind(Inventory));
Router.post('/', Inventory.create.bind(Inventory));
Router.delete('/:id', Inventory.delete.bind(Inventory));
Router.put('/:id', Inventory.update.bind(Inventory));

Router.post('/:id/transactions', Transactions.create.bind(Transactions));
Router.get('/:id/transactions', Transactions.getTransactions.bind(Transactions));
module.exports = Router;



