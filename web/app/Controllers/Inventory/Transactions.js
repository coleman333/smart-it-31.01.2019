const Controller = require('../Controller');
const GlobalModel = require('@model/index');
const Inventory = GlobalModel.inventory;
const Transaction = GlobalModel.transactions;
const Statuses = GlobalModel.statuses;

class TransactionsController extends Controller {
    create(Request, Response, next) {
      if(!Request.body.type_id || !Request.body.equipment_id) {
          Response.status(400).send({ success: false, message: '`type_id` and `equipment_id` is required parameters' });
          return;
      }

      const type_id = Request.body.type_id;
      const name = Request.body.name;
      const equipment_id = Request.body.equipment_id.replace(/^\D/, '');
      // console.log('================...........',equipment_id);
      const inventory_id = Request.params.id.replace(/^\D/, '');

      if(!equipment_id) {
        Response.status(404).send({ success: false, message: 'This inventory does not have an equipment' });
        return;
      }

      Inventory.findByPk(inventory_id).then((inventory) => {
        // console.log('================...........',inventory);

        // if(inventory.equipment_id !== +(equipment_id)) {
        //   Response.status(404).send({ success: false, message: 'Wrong equipment for this inventory' });
        //   return;
        // }
        Transaction.findOne({
          where: {
            inv_id: inventory_id
          },
          order: [['created_at', 'DESC']]
        }).then((trx) => {
          console.log('================...........',trx);

          if(trx && trx.type_id === Number(type_id)) {
            Response.status(406).send({ success: false, message: 'Duplicate `type` entity' });
            return;
          }

          if(!trx && Number(type_id) === 2) {
            Response.status(406).send({ success: false, message: 'You can not create demount transaction without mounting' });
            return;
          }
          Transaction.create({
            type_id: type_id,
            inv_id: inventory_id,
            name: name
          }).then((transaction) => {
            inventory.update({status_id: transaction.type_id});
            Response.send(transaction.prettify());
          }).catch((Error) => {
            next(Error);
          })
        })
      }).catch((error) => {
        next(error);
      });
    }

    getTransactions(Request, Response, next) {
      Transaction.findAll({
        inv_id: Request.params.id,
        include: [
          {
            model: Inventory,
            as: 'inventory'
          },
          {
            model: Statuses,
            as: 'status'
          }
        ],
        order: [['created_at', 'DESC']]
      }).then((transactions) => {
        Response.send(transactions);
      }).catch((error) => {
        next(error);
      })
    }
}

module.exports = TransactionsController;
