// import WarehouseBinding from '../../Models/warehouse_binding';
const Controller = require('@controller/Controller');
const GlobalModel = require('@model/index');
const Equipment = GlobalModel.equipment;
const Inventory = GlobalModel.inventory;
const InventoryMetadata = GlobalModel.inventory_metadata;
const Statuses = GlobalModel.statuses;
const Transaction = GlobalModel.transactions;
const WareHouseBinding = GlobalModel.warehouse_binding;
// const WarehouseBinding = require('../../Models/warehouse_binding');

const Schema = require('@schema/Inventory');

const TimeNormalization = require('@service/DefaultService').dateStringNormalization;
const moment = require('moment');

class InventoryController extends Controller {

    get(Request, Response) {
        Inventory.findById(Request.params.id, {
            include: [
                {
                    model: Statuses,
                    as: 'status'
                },
                {
                    model: InventoryMetadata,
                    as: 'metadata'
                },
                {
                    model: Equipment,
                    as: 'equipment',
                    paranoid: false
                },
                {
                    model: Transaction,
                    as: 'transactions',
                    order: [['created_at', 'DESC']]
                }
            ]
        }).then(async (invItem) => {
            if (!invItem) {
                Response.status(404).send({success: false, message: 'Nothing founded'});
                return;
            }
            Response.send(invItem);
        }).catch((Error) => {
            Response.status(500).send({success: false, error: Error});
        })
    }

    async countAllObjects(Request, Response, next){
        const inventoryCount = await Inventory.getInventoryQuantity();
        const equipmentCount = await Equipment.count({});
        const totalObjects = await +(inventoryCount[0].totalInventory) + (+equipmentCount);
        Response.status(200).json({totalObjects})
    }

    async countAll(Request, Response, next){
        const inventoryCount = await Inventory.getInventoryQuantity();
        Response.status(200).json({totalInventory:+(inventoryCount[0].totalInventory)})
    }

    async countAllMounted(Request, Response, next){
        const inventoryCountMounted = await Inventory.getInventoryQuantityMounted();
        Response.status(200).json({inventoryCountMounted:(inventoryCountMounted[0].mountedInventory)})
    }

    async countAllUnMounted(Request, Response, next){
        const inventoryCountUnMounted = await Inventory.getInventoryQuantityUnMounted();
        Response.status(200).json({inventoryCountUnMounted:(inventoryCountUnMounted[0].unMountedInventory)})
    }

    async countAllMarked(Request, Response, next){
        const inventoryCountMarked = await Inventory.count({where:{is_marked:true}});
        Response.status(200).json({inventoryCountMarked});
    }

    async countAllUnMarked(Request, Response, next){
        const inventoryCountUnMarked = await Inventory.count({where:{is_marked:false}});
        Response.status(200).json({inventoryCountUnMarked});
    }

    create(Request, Response, next) {
        this.Joi.validate(Request.body, Schema.create)
            .then((data) => {
            Inventory.create(data)
                .then((inv_value) => {
                Response.send(inv_value)
            }).catch(Error => {
                next(Error);
            });
        }).catch((error) => {
            next(error);
        });
    }

    async delete(Request, Response, next) {
        Inventory.findById(Request.params.id).then((inv) => {
            if (!inv) {
                Response.status(404).send({success: false, message: 'Inventory does not found'});
                return;
            }
            inv.destroy();
            Response.send(inv);
        }).catch((error) => {
            next(error);
        })
    }

    update(Request, Response, next) {
        this.Joi.validate(Request.body, Schema.update).then((data) => {
            Inventory.findById(Request.params.id)
                .then(async (item) => {
                item = Object.assign(item, data);
                item.save();
                Response.send(item);
            }).catch((error) => {
                next(error);
            })
        }).catch((invalid) => {
            next(invalid);
        });
    }

    getAll(Request, Response, next) {
        Inventory.findAll({include: [
                {
                    model: Statuses,
                    as: 'status'
                },
                {
                    model: InventoryMetadata,
                    as: 'metadata'
                },
                {
                    model: Equipment,
                    as: 'equipment',
                    paranoid: false
                },
                {
                    model: Transaction,
                    as: 'transactions',
                    order: [['created_at', 'DESC']]
                }
            ],
            order: [['created_at', 'DESC']]
        }).then((items) => {
            Response.send(items);
        }).catch((error) => {
            next(error);
        })
    }

    getAllWithFilterWithPagination(Request, Response, next){
        // const { doc_material ,doc_purchasing , factory , title , offset , limit } = Request.body;

        const { offset , limit  } = Request.body;
        const conditions = {};

        for (let key in Request.body) {
            if (Request.body[key]){
                conditions[key] = Request.body[key]
            }
        }

        InventoryMetadata.findAll({
            where:{ $and:
                    [
                        conditions.doc_material && {doc_material: {$like: `%${conditions.doc_material}%`}},
                        conditions.doc_purchasing && {doc_purchasing: {$like: `%${conditions.doc_purchasing}%`}},
                        conditions.check_date && {check_date: {$contains: [`${conditions.check_date}`]}},
                        conditions.provider_name && {provider_name: {$like: `%${conditions.provider_name}%`}},
                        conditions.income_order && {income_order: {$like: `%${conditions.income_order}%`}},
                        conditions.factory && {factory: {$like: `%${conditions.factory}%`}},
                        conditions.store_place && {store_place: {$like: `%${conditions.store_place}%`}},
                        conditions.consignment && {consignment: {$like: `%${conditions.consignment}%`}},
                        conditions.material && {material: {$like: `%${conditions.material}%`}},
                        conditions.title && {title: {$like: `%${conditions.title}%`}},
                        conditions.quantity && {quantity: {$like: `%${conditions.quantity}%`}},
                        conditions.ICP && {ICP: {$like: `%${conditions.ICP}%`}},
                        conditions.inventorization && {inventorization: {$like: `%${conditions.inventorization}%`}}
                    ]
        }, limit: Number(limit),
            offset: Number(offset),
        }).then((items) => {
            Response.send(items);
        }).catch((error) => {
            next(error);
        })
    }

    getQuantity(Request, Response, next) {
        try {
            const { consignment } = Request.params;
            Inventory.getQuantity(consignment).then(result => {
                if (result.length === 0) {
                    Response.status(404).send({success: false, message: 'Nothing founded by this consignment'});
                }
                const AllObjectsWithIds = result.map((i)=>{
                    const res = {};
                    const valuesMetadata = [];
                    const keysMetadata = [];
                    res.amount = i.amount;
                    res.ids = i.ids.split(',').map(id => `G${id}`);
                    res.title = i.title;
                    res.consignment = i.consignment;
                    res.material = i.material;
                    // let meta = JSON.parse(i.metadata);
                    let meta2 = {
                        doc_material: i.doc_material,
                        doc_purchasing: i.doc_purchasing,
                        check_date: i.check_date,
                        provider_name: i.provider_name,
                        income_order: i.income_order,
                        factory: i.factory,
                        store_place: i.store_place,
                        consignment: i.consignment,
                        material: i.material,
                        title: i.title,
                        evaluation_unit: i.evaluation_unit,
                        quantity: i.quantity,
                        ICP: i.ICP,
                        inventorization: i.inventorization
                    };
                    Object.keys(meta2).forEach((item) => {
                        keysMetadata.push(item);
                        valuesMetadata.push(meta2[item]);
                    });
                    res.keysMetadata = keysMetadata;
                    res.valuesMetadata = valuesMetadata;
                    return res;
                });
                Response.status(200).send({success: true, AllObjectsWithIds});
                return next();
            }).catch((error) => {
                next(error);
            })
        } catch (error) {
            console.log(error);
        }
    }

    async markInventory(Request, Response, next){
        const id = Request.params.id.replace(/^\D/, '');
        const inventory = await Inventory.findByPk(id);
        if (!inventory) {
            Response.status(404).send({success: false, message: 'Inventory does not found'});
            return next();
        }
        if (inventory.is_marked) {
            Response.status(406).send({success: false, message: 'Inventory already marked'});
            return next();
        }
        inventory.is_marked = true;
        await inventory.save();
        Response.send(inventory.prettify());
        return next();
    }

    async bindInventoryToWareHouse(Request, Response, next){
        try {
            let { inventory_id, warehouse_id, MacAddress} = Request.body;
            inventory_id = inventory_id.replace(/^\D/, '');
            const res = await WareHouseBinding.find({where:{inventory_id:inventory_id}});
            if(res !== null){
                Response.status(406).send({success: false,  message:`${Request.body.inventory_id} has already add to warehouse ${warehouse_id}`});
            }
            else{
                const result = await WareHouseBinding.create({inventory_id: inventory_id, warehouse_id: warehouse_id });
                Response.status(200).send({success: true,  message:`${Request.body.inventory_id} added to warehouse ${warehouse_id}`});
            }
        } catch (e) {
            console.log(e);
        }
    }

    async allMarkedDetailsByDate(Request, Response, next){
        const { date_start, date_end} = Request.body;


    }

    // getServeDate(Request, Response, next) {
    //     const id = Request.params.id.slice(1);
    //     const arrayOfMilliseconds =[];
    //     Transaction.findAll({
    //         where: {
    //             inv_id: id,
    //             type_id: 1
    //         },
    //         order: [['created_at', 'DESC']]
    //     }).then((mountTrx) => {
    //         // console.log('==================>',mountTrx);
    //         if (!mountTrx) {
    //             Response.status(404).send({
    //                 success: false,
    //                 message: 'Mount transaction does not exists yet'
    //             });
    //             return;
    //         }
    //         Transaction.findAll({
    //             where: {
    //                 inv_id: id,
    //                 type_id: 2
    //             },
    //             order: [['created_at', 'DESC']]
    //         }).then((demountTrx) => {
    //             // console.log('==================>',demountTrx);
    //
    //             let lastDate;
    //             mountTrx.map((item,index)=>{
    //                 if (!demountTrx) {
    //                     lastDate = moment();
    //                 } else {
    //                     lastDate = demountTrx[index].created_at;
    //                 }
    //                 // console.log('==================>',lastDate);
    //
    //                 const firstDate = mountTrx[index].created_at;
    //                 console.log('==================>',firstDate,lastDate);
    //                 // moment.utc(lastDate.diff(firstDate))
    //                 // const differ = moment.utc(lastDate.diff(firstDate));
    //                 // let differ = lastDate.diff(firstDate);
    //                 //
    //                 console.log('==================> EEEE ');
    //                 let a = moment()+8000;
    //                 console.log('==================> EEEE ', moment(lastDate).diff(moment(firstDate)));
    //                 // const duration = moment.duration(lastDate.diff(firstDate));
    //                 // console.log('==================>',firstDate,lastDate);
    //
    //                 arrayOfMilliseconds.push(duration);
    //             });
    //             let allMilliseconds=0;
    //             arrayOfMilliseconds.map((item)=>{
    //                  moment(allMilliseconds).add(item, 'milliseconds')
    //             });
    //
    //
    //
    //             Response.send(TimeNormalization(allMilliseconds));
    //         }).catch((error) => {
    //             next(error);
    //         })
    //     }).catch((Error) => {
    //         next(Error);
    //     })
    // }

    async getServeDate(Request, Response, next) {
        try {
            const id = Request.params.id.slice(1);
            const mounts = await Transaction.findAll({
                where: {
                    inv_id: id,
                    type_id: {
                        $in: [1, 2]
                    },
                },
                order: ['created_at']
            }) || [];
            if (mounts === []) {
                Response.status(404).send({
                    success: false,
                    message: 'Mount transaction does not exists yet'
                });
                return;
            }
            let time = 0;
            const lastIndex = mounts.length - 1;
            for (let i = 0; i <= lastIndex; i += 2) {
                const mountDate = mounts[i].created_at;
                let unmountDate;
                if (i + 1 <= lastIndex) {
                    unmountDate = mounts[i + 1].created_at;
                } else {
                    unmountDate = Date.now();
                }

                time+=Math.abs(moment(mountDate).diff(unmountDate,'milliseconds'));
            }
            let duration = moment.duration(time);
            Response.json(TimeNormalization(duration));

        }catch (e) {
            console.error(e);
            next(e)
        }
    }
}

module.exports = InventoryController;
