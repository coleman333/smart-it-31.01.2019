const express = require('express');
const Router = express.Router();
const VendorCodeSearchController = require('@controller/VendorCodeSearch/vendorCodeSearch');
const VendorCodeSearch = new VendorCodeSearchController();

Router.get('/:id', VendorCodeSearch.get.bind(VendorCodeSearch));

module.exports = Router;
