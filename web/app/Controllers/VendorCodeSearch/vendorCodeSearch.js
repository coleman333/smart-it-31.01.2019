const Controller = require('@controller/Controller');
const GlobalModel = require('@model/index');
const Equipment = GlobalModel.equipment;
const Inventory = GlobalModel.inventory;
const Statuses = GlobalModel.statuses;
const Inventory_Metadata = GlobalModel.inventory_metadata;
const Transaction = GlobalModel.transactions;
const Schema = require('@schema/Inventory');


class VendorCodeSearchController extends Controller{

    get(Request, Response, next) {

        const id = Request.params.id.slice(1);
        if (Request.params.id.match(/^(g|G)/)) {
            Inventory.findById(id, {
                include: [
                    {
                        model: Statuses,
                        as: 'status'
                    },
                    {
                        model: Equipment,
                        as: 'equipment',
                        paranoid: false
                    },
                    {
                        model: Inventory_Metadata,
                        as: 'metadata'
                    },
                    {
                        model: Transaction,
                        as: 'transactions',
                        // order: [['created_at', 'DESC']]
                    }
                ]
            }).then(async (invItem) => {
                if (!invItem) {
                    Response.status(404).send({success: false, message: 'Nothing founded'});
                    return;
                }

                Response.send(invItem.prettify());
            }).catch((Error) => {
                console.log(Error);
                Response.status(500).send({success: false, error: Error});
            })
        } else if (Request.params.id.match(/^(e|E)/)) {
            Equipment.findByPk(id, {
                include: [
                    {
                        model: Inventory,
                        as: 'inventory',
                        include: [
                            {
                                model: Statuses,
                                as: 'status',
                            },
                            {
                                model: Inventory_Metadata,
                                as: 'metadata',
                            }
                        ]
                    }
                ]
            }).then(async (eqItem) => {
                if (!eqItem) {
                    Response.status(404).send({success: false, message: 'Nothing founded'});
                    return;
                }

                Response.send(eqItem.prettify());
            }).catch((Error) => {
                Response.status(500).send({success: false, error: Error});
            })
        } else {
            Response.status(404).send({success: false, message: 'Nothing founded'});
            return;
        }

    }
}

module.exports = VendorCodeSearchController;
