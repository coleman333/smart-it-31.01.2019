const Controller = require('../app/Controllers/Controller');
const fs = require('mz/fs');
const Excel = require('exceljs');
const _ = require('lodash');
const async = require('async');
const util = require('util');
const GlobalModel = require('@model/index');
const Inventory = GlobalModel.inventory;
const Inventory_Metadata = GlobalModel.inventory_metadata;
const Equipment = GlobalModel.equipment;

const itemProcessing = (item) => {
    return Object.keys(item).reduce((obj, key) => {
        if (typeof item[key] === 'string') {
            obj[key] = item[key].trim();
        } else {
            obj[key] = item[key];
        }
        return obj;
    }, {});
};

class xlxsParserForTestsController extends Controller {

    async uploadFile(){
        try {
            const xml = await fs.readFile('./XML.xlsx');
            const workbook = await new Excel.Workbook();
            await workbook.xlsx.load(xml);
            await util.promisify(async.each).call(
                async,
                workbook.worksheets,
                async function (worksheet, sheetId) {
                    try {
                        const columns = _.compact(worksheet.getRow(1).values);
                        const rows = worksheet.getSheetValues();
                        const results = _.chain(rows)
                            .compact()
                            .tail()
                            .filter(r => !_.isEmpty(r))
                            .map(row => _.zipObject(columns, _.tail(row)))
                            .value();

                        if (worksheet.name.match(/Приходный ордер/)) {
                            await util.promisify(async.each).call(
                                async,
                                results,
                                async rowItem => {
                                    const item = itemProcessing(rowItem);

                                    const metadata = {
                                        title: item['Опис матеріалу'],
                                        consignment: item['Партія'],
                                        material: item['Матеріал'],
                                        metadata: item
                                    };
                                    if (!metadata.title) {
                                        return;
                                    }

                                    const meta = await Inventory_Metadata.create(metadata);
                                    const amount = item['Кількість'] || 1;

                                    return util.promisify(async.times).call(
                                        async,
                                        amount,
                                        async () => Inventory.create({metadata_id: meta.id})
                                    );
                                }
                            );
                        } else if (worksheet.name.match(/Единицы оборудования/)) {
                            await util.promisify(async.each).call(
                                async,
                                results,
                                async rowItem => {
                                    const item = itemProcessing(rowItem);
                                    return Equipment.create({
                                        name: item['Опис'],
                                        equipment_id: item['Единица оборудования'],
                                        inventory_id: item['Інвентарн.номер'],
                                        metadata: item
                                    });
                                }
                            );
                        }

                    } catch (e) {
                        console.error(e);
                    }
                }
            );
        } catch (e) {
            console.log(`Error in file`, e);
        }
    };

}
module.exports = xlxsParserForTestsController;
