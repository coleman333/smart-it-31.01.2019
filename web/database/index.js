module.exports = {
    equipment          : require('./migrations/1547126335189_create_equipment_table'),
    statuses           : require('./migrations/1547126335190_create_statuses_table'),
    inventory_metadata : require('./migrations/1547126353000_create_inventory_metadata_table'),
    inventory          : require('./migrations/1547126483131_create_inventory_table'),
    transaction        : require('./migrations/1547127356201_create_transactions_table'),
    warehouse_binding  : require('./migrations/1551259547821_create_warehouse_binding'),
    warehouse          : require('./migrations/1551259525333_create_warehouse'),
    company            : require('./migrations/1547127356202_create_company_table')
};

// default_statuses   : require('./migrations/1547127704474_create_default_statuses'),


