module.exports = {
    "up": (con, cb) => {
        con.query(
            `
                CREATE TABLE equipment(
                  id BIGINT UNSIGNED AUTO_INCREMENT,
                  name VARCHAR(256) NOT NULL,
                  equipment_id VARCHAR(256) NOT NULL, 
                  inventory_id VARCHAR(256) NOT NULL,
                  is_marked BOOLEAN DEFAULT 0,
                  metadata JSON,
                  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                  deleted_at TIMESTAMP NULL,
                  PRIMARY KEY(id)
                  
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=100000;
            `,
            (err, res) => {
                if(!err) {
                    con.query(
                        `
                            INSERT INTO equipment (name, equipment_id, inventory_id, is_marked)
                            values('equipment0' , '0' ,'0' ,false  );
                        `,
                        (err, res) => {
                            if(err) {
                                console.log(err);
                            }
                            cb();
                        }
                    )
                } else {
                    console.log(err);
                    cb();
                }
            }
        )
    },
    "down": `
        DROP TABLE equipment;
    `
}
//add foreign key to equipment and inventory id
