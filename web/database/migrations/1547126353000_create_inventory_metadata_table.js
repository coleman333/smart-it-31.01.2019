module.exports = {
    "up": `
        CREATE TABLE inventory_metadata (
          id BIGINT UNSIGNED AUTO_INCREMENT UNIQUE,
          doc_material VARCHAR(256),
          doc_purchasing VARCHAR(256),
          check_date DATE ,
          provider_name VARCHAR(256),
          income_order VARCHAR(256),
          factory VARCHAR(256),
          store_place VARCHAR(256),
          consignment VARCHAR(256),
          material VARCHAR(256),
          title VARCHAR(256),
          evaluation_unit VARCHAR(256),
          quantity VARCHAR(256),
          ICP VARCHAR(256),
          inventorization VARCHAR(256),
          
--           title VARCHAR(256) NOT NULL,
--           consignment VARCHAR(256) NOT NULL,
--           material INT NOT NULL,
--           metadata JSON,
          created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
          deleted_at TIMESTAMP NULL,
          PRIMARY KEY(id)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1;
    `,
    "down": `
        DROP TABLE inventory_metadata;
    `
}