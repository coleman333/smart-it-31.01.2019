module.exports = {
    "up": `
    CREATE TABLE company (
      id INT AUTO_INCREMENT,
      company_name VARCHAR(255) NULL,
      PRIMARY KEY(id)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
    `,
    "down": `
        DROP TABLE company;
    `
}
