module.exports = {
  "up": `
    CREATE TABLE inventory (
      id BIGINT UNSIGNED AUTO_INCREMENT UNIQUE,
      metadata_id BIGINT UNSIGNED,
      is_marked BOOLEAN NOT NULL DEFAULT 0,
      status_id INT,
      equipment_id BIGINT UNSIGNED,
      created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
      deleted_at TIMESTAMP NULL,          
      PRIMARY KEY(id),
      FOREIGN KEY (status_id) REFERENCES statuses(id) ON DELETE SET NULL,
      FOREIGN KEY (equipment_id) REFERENCES equipment(id) ON DELETE CASCADE,
      FOREIGN KEY (metadata_id) REFERENCES inventory_metadata(id) ON DELETE SET NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=100000;
    `,
  "down": `
        DROP TABLE inventory;
    `
}