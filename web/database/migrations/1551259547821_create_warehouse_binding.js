module.exports={
    "up":`CREATE TABLE warehouse_binding(
        id INT AUTO_INCREMENT,
        inventory_id BIGINT UNSIGNED,
        warehouses_id INT NOT NULL, 
        date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY(id),
        FOREIGN KEY (inventory_id) REFERENCES inventory(id) ON DELETE CASCADE, 
        FOREIGN KEY (warehouses_id) REFERENCES warehouse(id) ON DELETE CASCADE
    );
    `,
    "down": `
        DROP TABLE warehouse_binding;
    `
}
