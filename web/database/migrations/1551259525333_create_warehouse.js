module.exports = {
    "up": `
    CREATE TABLE warehouse (
      id INT AUTO_INCREMENT,
      warehouse_name VARCHAR(255),
      location VARCHAR(255),
      storekeeper_name VARCHAR(255),
      storekeeper_second_name VARCHAR(255),
      macAddress VARCHAR(255), 
      company_id INT NOT NULL,
      PRIMARY KEY(id),         
      FOREIGN KEY (company_id) REFERENCES company(id) ON DELETE CASCADE
    ); 
    `,
    "down": `
        DROP TABLE warehouse;
    `
}
