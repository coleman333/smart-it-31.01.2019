module.exports = {
  "up": (con,cb)=>{
    con.query(
        `CREATE TABLE statuses(
          id INT AUTO_INCREMENT,
          name VARCHAR(255) NOT NULL,
          PRIMARY KEY(id)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1;
      `,
        (err, res) => {
          if(!err) {
            con.query(
                `
                            INSERT INTO statuses (name) values('1'),('2');
                        `,
                (err, res) => {
                  if(err) {
                    console.log(err);
                  }
                  cb();
                }
            )
          } else {
            console.log(err);
            cb();
          }
        }
    )
  },

  "down": `
        DROP TABLE statuses;
    `
}
