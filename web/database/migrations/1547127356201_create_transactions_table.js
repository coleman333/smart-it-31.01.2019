module.exports = {
  "up": `
        CREATE TABLE transactions(
          inv_id BIGINT UNSIGNED NOT NULL,
          type_id INT NOT NULL,
          created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
          deleted_at TIMESTAMP NULL,
          name VARCHAR(255),
          PRIMARY KEY(inv_id, type_id, created_at),
          FOREIGN KEY(inv_id) REFERENCES inventory(id) ON DELETE CASCADE,
          FOREIGN KEY(type_id) REFERENCES statuses(id) ON DELETE CASCADE 
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1;
    `,
  "down": `
        DROP TABLE transactions;
    `
};
