const express = require('express');
const Router = express.Router();

Router.use('/vendor_code_search',require('./../app/Controllers/VendorCodeSearch/routes'));
Router.use('/inventory', require('./../app/Controllers/Inventory/routes'));
Router.use('/equipment', require('./../app/Controllers/Equipment/routes'));
Router.use('/device', require('./../app/Controllers/Device/routes'));
Router.use('/docs', require('./../app/Controllers/Swagger/routes'));

module.exports = Router;

